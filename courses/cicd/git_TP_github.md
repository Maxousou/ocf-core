title:GIT - TP Github
intro:nous permettra de manipuler Github et en particulier les Pages.
conclusion:Vu une utilisation possible de Github.

---

## Création d'un projet, utilisation d'une page Github

- Suivre les consignes de ce dépôt : https://github.com/tsaquet/tp_git 

- Une fois que c'est fait, créer une page Github en s'inspirant de que l'on trouve ici : 
  - https://github.com/JamesIves/github-pages-deploy-action 
