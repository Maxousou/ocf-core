title:Github - Gitlab
intro:nous introduira ce que sont ces plateformes
conclusion:Eu un aperçu des possibilités offertes par Github et Gitlab

---

## Github

### Une révolution

- Le « Google » du développeur : Outil majeur de montée en compétence
- Centré sur l'humain
- Encourage l'amateurisme


---

## Gitlab

### Le petit frère de GitHub

- Quelques fonctions en moins : 
  - La recherche parmi toutes les contributions
  - Github actions
- Quelques fonctions en plus :
  - Version self-hosted open source
  - Gitlab CI

---

## Gitlab

- Le produit
  - Serveur de gestion de source Git et de CI/CD
  - Existe en version open source
- Principales fonctionnalités
  - Serveur de versionning Git
  - CI / CD / CD (Auto DevOps)
  - Intégration LDAP / AD
  - Gestion de projets et tickets
- Avantages
  - Pas d'add-on à maintenir (vs forge multi-outils)
  - Synergie inter-outils élevée